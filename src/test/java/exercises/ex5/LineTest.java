package exercises.ex5;

import org.junit.Assert;
import org.junit.Test;

public class LineTest {

/**
 * Tests for Line.java should be implemented here
 * Create tests for the getSlope, getDistance and parallelTo methods,
 * use one of the libraries: Junit, TestNg, AssertJ.
 *
 *
 * Because of rounding errors, it is bad practice to test double values for exact equality.
 * To get around this, you can pass a small delta value to assertEquals. What can be done to avoid comparing double values? Please
 */

    @Test
    public void getSlopeTest(){
        Line l = new Line(2.0, 2.0, 4.0, 4.0);
        double slope = l.getSlope();

        Assert.assertEquals(slope, 1.0, .00001);
    }

    @Test
    public void getDistance(){
        Line l = new Line(2.0, 2.0, 4.0, 4.0);
        double dist = l.getDistance();

        Assert.assertEquals(dist, 2*Math.sqrt(2), .00001);
    }

    @Test
    public void parralelTo(){
        Line l1 = new Line(2.0, 2.0, 4.0, 4.0);
        Line l2 = new Line(3.0, 3.0, 5.0, 5.0);
        boolean parralelity = l1.parallelTo(l2);

        Assert.assertTrue(parralelity);
    }

}


