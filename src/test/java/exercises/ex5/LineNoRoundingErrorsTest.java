package exercises.ex5;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class LineNoRoundingErrorsTest {

    @Test
    public void getSlopeTest(){
        LineNoRoundingErrors l = new LineNoRoundingErrors(2.0, 2.0, 4.0, 4.0);
        double slope = l.getSlope();

        Assert.assertEquals(slope, 1.0, .00001);
    }

    @Test
    public void getDistance(){
        LineNoRoundingErrors l = new LineNoRoundingErrors(2.0, 2.0, 4.0, 4.0);
        double dist = l.getDistance();

        Assert.assertEquals(dist, 4*Math.sqrt(2), .00001);
    }

    @Test
    public void parralelTo(){
        LineNoRoundingErrors l1 = new LineNoRoundingErrors(2.0, 2.0, 4.0, 4.0);
        LineNoRoundingErrors l2 = new LineNoRoundingErrors(3.0, 3.0, 5.0, 5.0);
        boolean parralelity = l1.parallelTo(l2);

        Assert.assertTrue(parralelity);
    }
}