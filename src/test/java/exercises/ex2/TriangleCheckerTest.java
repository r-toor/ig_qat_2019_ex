package exercises.ex2;

import org.junit.Assert;
import org.junit.Test;

/**
 * unit tests for TriangleChecker should be implemented here
 */
public class TriangleCheckerTest {

    @Test
    public void testEquilateral() throws NotATriangleException {
        TriangleChecker t = new TriangleChecker();
        Triangle result = t.checkTriangleType(2,2,2);
        Assert.assertEquals(result, Triangle.EQUILATERAL);
    }

    @Test
    public void testIsosceles1() throws NotATriangleException {
        TriangleChecker t = new TriangleChecker();
        Triangle result = t.checkTriangleType(4,3,3);
        Assert.assertEquals(result, Triangle.ISOSCELES);
    }

    @Test
    public void testIsosceles2() throws NotATriangleException {
        TriangleChecker t = new TriangleChecker();
        Triangle result = t.checkTriangleType(2,3,3);
        Assert.assertEquals(result, Triangle.ISOSCELES);
    }

    @Test
    public void testOther() throws NotATriangleException {
        TriangleChecker t = new TriangleChecker();
        Triangle result = t.checkTriangleType(4,3,5);
        Assert.assertEquals(result, Triangle.OTHER);
    }

    @Test(expected = NotATriangleException.class)
    public void testNotTriangle() throws NotATriangleException {
        TriangleChecker t = new TriangleChecker();
        t.checkTriangleType(1,1,3);

    }

    @Test(expected = NotATriangleException.class)
    public void testNotTriangleNegative() throws NotATriangleException {
        TriangleChecker t = new TriangleChecker();
        t.checkTriangleType(-5,5,5);

    }


}