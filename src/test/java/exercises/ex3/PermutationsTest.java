package exercises.ex3;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.FileNotFoundException;


public class PermutationsTest {

    private static final String DICT_PATH = "src/main/java/exercises/ex3/resources/odm.txt";

    @Test
    public void test1WithoutValidPerm() throws FileNotFoundException {
        Permutations p = new Permutations();
        p.printPermutations("AAlto");

        Assert.assertEquals(p.getPermutator().getCombinations(), 5*4*3*2);
    }

    @Test
    public void test2WithoutValidPerm() throws FileNotFoundException {
        Permutations p = new Permutations();
        p.printPermutations("Barbara");

        Assert.assertEquals(p.getPermutator().getCombinations(), 7*6*5*4*3*2);
    }

    @Test
    @Ignore
    public void test1WithValidPerm() throws FileNotFoundException {
        Permutations p = new Permutations(DICT_PATH);
        p.printPermutations("AAlto");

        Assert.assertEquals(p.getPermutator().getCombinations(), 5*4*3*2);
    }

    @Test
    @Ignore
    public void test2WithValidPerm() throws FileNotFoundException {
        Permutations p = new Permutations(DICT_PATH);
        p.printPermutations("Barbara");

        Assert.assertEquals(p.getPermutator().getCombinations(), 7*6*5*4*3*2);
    }





}