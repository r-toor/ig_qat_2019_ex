package exercises.ex3;

import org.junit.Assert;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.util.Set;

public class WordUploaderTest {

    private static final String TEST_PATH = "src/main/java/exercises/ex3/resources/testfile";

    @Test
    public void parseValidWordsInLines() throws FileNotFoundException {
        WordUploader w = new WordUploader(TEST_PATH);
        Set<String> uploaded = w.uploadValidWords("Abababab");

        Assert.assertTrue(uploaded.contains("Balborgu"));
    }
}