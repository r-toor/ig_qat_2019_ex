package exercises.ex4;

import org.junit.Test;

import static org.junit.Assert.*;

public class PalindromeTest {

    @Test
    public void palindromeEven(){
        PalindromeCheckerInterface p = new Palindrome();
        assertTrue(p.isPalindrome("qwertyytrewq"));
    }

    @Test
    public void palindromeOdd(){
        PalindromeCheckerInterface p = new Palindrome();
        assertTrue(p.isPalindrome("qwertytrewq"));
    }

    @Test
    public void notPalindrome(){
        PalindromeCheckerInterface p = new Palindrome();
        assertFalse(p.isPalindrome("qwertytrwq"));
    }
}