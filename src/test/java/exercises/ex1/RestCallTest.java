package exercises.ex1;

import io.restassured.response.ValidatableResponse;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.RestAssured.when;

/**
 * The purpose of this test is to check that unauthorized user is not able to get pending agreements.
 *
 * 1. Send GET request without headers to https://api.ig.com/usermanagement/pendingagreements. Please use REST-assured (http://rest-assured.io) to do it.
 * 2. Check if:
 *    - response code is 401
 *    - global error message is: "error.security.client-token-missing"
 * 
 * You can additinally use these libraries: Junit, TestNg, AssertJ  
 */
public class RestCallTest {

    @Test
    public void invalidRequest(){
        ValidatableResponse r = when().get("https://api.ig.com/usermanagement/pendingagreements")
                .then()
                .statusCode(401);

        String path = r.log().ifError().extract().body().jsonPath().getString("globalErrors");
        Assert.assertEquals(r.extract().statusCode(), 401);
        Assert.assertTrue(path.contains("error.security.client-token-missing"));
    }
}
