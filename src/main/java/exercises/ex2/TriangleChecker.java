package exercises.ex2;

import java.util.Arrays;

/**
 * The goal of this exercise is to implement a method below which checks triangle type out of given sides: int a, int b, int c
 * or throws NotATriangleException.
 * You should also implement unit tests verifying this method
 */

public class TriangleChecker {
    public Triangle checkTriangleType(int a, int b, int c) throws NotATriangleException {
        int[] sides = {a, b, c};
        Arrays.sort(sides);
        if(isNonnegative(sides) && isATriangle(sides)){

            if( sides[0] == sides[1] && sides[0] == sides[2])
                return Triangle.EQUILATERAL;
            else if (sides[0] == sides[1] || sides[1] == sides[2])
                return Triangle.ISOSCELES;
            else
                return Triangle.OTHER;

        } else throw new NotATriangleException();

    }

    private boolean isNonnegative(int[] sides){
        return Arrays.stream(sides).allMatch(s -> s > 0);
    }

    private boolean isATriangle(int[] sides){
        if(sides[0] + sides[1] > sides[2]) return true;
        else return false;
    }

}
