package exercises.ex5;

public class LineNoRoundingErrors {

   /**
    *
    * What can be done to avoid comparing double values? Change below code to avoid rounding errors of double values
    */



   public LineNoRoundingErrors(double x0, double y0, double vectorX, double vectorY) {
      if((int)vectorX* accurancyMultiplier == 0) throw new ArithmeticException( );
      this.x0 = x0;
      this.y0 = y0;
      this.x1 = x0 + vectorX;
      this.y1 = y0 + vectorY;
   }

   // calculates the slope of the line
   public double getSlope( ) {
      return (y1 - y0) / (x1 - x0);
   }

   // calculates the distance of the line
   public double getDistance( ) {
      return Math.sqrt(Math.pow((x1 - x0), 2) + Math.pow((y1 - y0), 2));
   }

   // returns true if a line is parallel to another
   public boolean parallelTo(LineNoRoundingErrors l) {
      // if the difference between the slopes is very small, consider them parallel

      int thisSlope = (int) getSlope()* accurancyMultiplier;
      int lSlope = (int) l.getSlope()* accurancyMultiplier;

      if(thisSlope == lSlope) {
         return true;
      } else {
         return false;
      }
   }

   private double x0, y0, x1, y1;
   private int accurancyMultiplier = 1_000_000;
}
