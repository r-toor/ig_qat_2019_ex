package exercises.ex3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WordUploader {

    private String path;

    public WordUploader(String path){
        this.path = path;
    }

    public Set<String> uploadValidWords(String word) throws FileNotFoundException {

        Stream<Character> charStream = word.toLowerCase().chars().mapToObj(i->(char)i);
        Character[] letters_arr = charStream.toArray(Character[]::new);
        int word_len = letters_arr.length;

        Set<Character> letters = new HashSet<>(Arrays.asList(letters_arr));
        BufferedReader dict_reader = new BufferedReader(new FileReader(path));
        Set<String> validWords = new HashSet<>();

        Set<String> validLines = dict_reader.lines().filter(line -> {
            for(char l: letters){
                if(line.toLowerCase().charAt(0) == l) return true;
            }
            return false;
        }).collect(Collectors.toSet());

        for(String line: validLines){
            String[] words = line.split(", ");
            for(String w: words){
                if(w.length() == word_len)
                    validWords.add(w);
            }
        }
        return validWords;
    }

}
