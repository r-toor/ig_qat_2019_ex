package exercises.ex3;

import java.io.FileNotFoundException;
import java.util.Set;
import java.util.stream.Collectors;

/**
   As an input to this program, you provide word
   As a result it is expected that all permutations of letters in this word will be printed out as well as number of permutations
   Optional : verify if each permutation of a word is a valid word in https://sjp.pl/ dictionary

   Use Permutator class
   Add unit tests to validate if the program works properly
 */
public class Permutations {

   private Permutator permutator;
   private WordUploader uploader;

   public Permutations(){}

   public Permutations(String path){
      uploader = new WordUploader(path);
   }

   public void printPermutations(String word) throws FileNotFoundException {
      if(uploader == null){
         printWithoutValidPerm(word);
      } else {
         printWithValidPerm(word);
      }

   }

   private void printWithValidPerm(String word) throws FileNotFoundException {
      permutator = new Permutator();
      Set<String> found = permutator.findAllPermutations(word);
      Set<String> valid = uploader.uploadValidWords(word);
      Set<String> validPerms = validWords(found, valid);

      System.out.println("Permutations: ");
      for(String w: found){
         System.out.println(w);
      }

      System.out.println("Valid: " + validPerms.size());
      for(String w: validPerms){
         System.out.println(w);
      }
   }

   private void printWithoutValidPerm(String word){
      permutator = new Permutator();

      Set<String> found = permutator.findAllPermutations(word);

      System.out.println("Permutations: ");
      for(String w: found){
         System.out.println(w);
      }

   }

   private Set<String> validWords(Set<String> permutations, Set<String> validWords){
       Set<String> validPerm = validWords.stream().filter(word ->
         permutations.stream().anyMatch(perm -> perm.equalsIgnoreCase(word))
      ).collect(Collectors.toSet());

      for(String vp: validPerm)
         System.out.println(vp);

       return validPerm;
   }


   public Permutator getPermutator() {
      return permutator;
   }
}
