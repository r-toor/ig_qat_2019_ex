package exercises.ex3;

import java.util.HashSet;
import java.util.Set;

public class Permutator {

   private int combinations;

   public void increment(){
      combinations++;
   }

   public int getCombinations() {
      return combinations;
   }

   public Set<String> findAllPermutations(String word){
      Set<String> words = new HashSet<>();
      char[] word_arr = word.toCharArray();

      permutationsUtil(word_arr, word_arr.length, words);

      return words;
   }

   private void permutationsUtil(char[] word_arr, int length, Set<String> words){
      if(length == 1) {
         words.add(String.valueOf(word_arr));
         this.increment();
      } else {
         for(int i=0; i<length; i++){
            swap(word_arr, i, length-1);
            permutationsUtil(word_arr, length-1, words);
            swap(word_arr, i, length-1);

         }
      }
   }

   private void swap(char[] arr, int i1, int i2){
      char tmp = arr[i1];
      arr[i1] = arr[i2];
      arr[i2] = tmp;

   }
}
