package exercises.ex4;

/**
   Create application that will check if a given String is a palindrome or not
   As a result we should receive boolean value if the input word is a palindrome or not

   Add Junit tests that will verify if the application is written in a proper way

   Mandatory: Palindrome class must implement the PalindromeCheckerInterface
 */
public class Palindrome implements PalindromeCheckerInterface {
    @Override
    public boolean isPalindrome(String theWord) {
        char[] theWord_arr = theWord.toCharArray();
        int i=0, j=theWord_arr.length-1;

        while(i < j){
            if(theWord_arr[i++] != theWord_arr[j--])
                return false;
        }
        return true;
    }

    @Override
    public void printResult() {

    }
}
