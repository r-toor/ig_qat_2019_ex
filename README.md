In exercise 3, there are functions, that checks validity of each permutation, 
however file with all valid words isn't uploaded to repository 
because of size.

In class main/test/java/exercises/ex3/PermutationsTest there is default path of dictionary file,
and tests, which uses that, are annotated as @Ignore.

In order to test checking validation, first you have to download dictionary file,
and optionally set variable DICT_PATH, if name of file is different.